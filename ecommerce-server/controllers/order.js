// ORDER CONTROLLER
const Order = require("../model/Order");
const User = require("../model/User");
const Product = require("../model/Product");

//1. REGISTER ORDER
module.exports.registerOrder = async (userData, reqBody) => {
	if(userData.isAdmin === false){
		let userID = await User.findOne({_id: userData.id}).then(result => {
			return result.id;
		});

		let productID = await Product.findOne({_id: reqBody.productId}).then(result => {
			return result.id;
		});

		let itemName = await Product.findOne({_id: reqBody.productId}).then(result => {
			return result.name;
		});

		console.log(itemName);
		let price = await Product.findOne({_id: reqBody.productId}).then(result => {
			return result.price;
		});

		let newOrder = new Order ({
			productName: itemName,
			productId: productID,
			userId: userID,
			totalAmount: price * reqBody.quantity
		});

		User.findOne({_id: userData.id}).then(result => {
			result.Orders.push(newOrder.id)

			return result.save().then((result, error) =>{
				if(error){
					return `failed.`;
				} else{
					return `Success.`;
				}
			})
		})

		Product.findOne({_id: reqBody.productId}).then(result => {
			result.thoseWhoOrdered.push(newOrder.id)

			return result.save().then((result, error) =>{
				if(error){
					return `failed`;
				} else{
					return `Success.`;
				}
			})
		})

		return newOrder.save().then((result, error) => {
			if(error){
				return `failed`;
			} else{
				return `Success.`;
			}
		})

	}else{
		return `You are an admin and therefore cannot order a product.`;
	}
};



//2. RETRIEVE ALL ORDERS (AN ADMIN ONLY FEATURE)
module.exports.retrieveAllOrders = async () => {
	
	return Order.find({}).then(result => {
			// Returns the user information with the password as an empty string
			return result;
		})
	

};


//3. RETRIEVE AUTHENTICATED USER'S ORDERS
module.exports.getMyOrders = async (userData) => {
	if(userData.isAdmin == false) {
		return Order.findOne({userId: userData.id}).then(result => {
			if (result == null){
				return "You have no items in your cart."
			}
			else{
				return result;
			}
			console.log(result);
		})
	}
	else{
		return "Please login."
	}
}


