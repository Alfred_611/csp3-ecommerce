const Order = require ("../models/Order")
const Product= require("../models/Product")
const User= require ("../models/User")
const Cart = require ("../models/Cart")
const bcrypt = require("bcrypt");
const auth = require("../auth");

// order model adding data
module.exports.getUserCart = async (reqBody) => {
	let productId = reqBody.productId;
	let userId = reqBody.userId;
	let productName = reqBody.productName;
	let price = reqBody.quantity;
	let quantity = reqBody.quantity;
	let subTotal = quantity*price;
	let total = subTotal;
	let newCart = new Cart ({
		userId:userId;
		total:total,
		products :{
			productId:productId,
			productName:productName,
			price:price,
			quantity:quantity,
			subTotal:subTotal

		}

	})
	console.log(newCart)
	return newCart.save().then((result,error) =>{
		if (error) {
			return false;
		}else{
			return true;
		}
	})
	.catch(err => {
		console.log(err); // Remember to thandle error to avoid hanging the request
		return false;
	});
}