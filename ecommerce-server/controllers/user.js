// USER CONTROLLER
const User = require("../model/User"); //need ilink sila model
const Product = require("../model/Product");
const Order = require("../model/Order");
const bcrypt = require("bcrypt");
const auth = require("../auth"); 


// Check if the email already exists
module.exports.checkEmailExists = (reqBody) => {	
	return User.find({email: reqBody.email}).then(result => {

		if(result.length > 0){
			return true;
		}
		else{
			return false;
		}
	})
}


// REGISTER USER
module.exports.registerUser = (reqBody) => {
	// Creates a variable "newUser" and instantiates a new "User" object using the mongoose model
	// Uses the information from the request body to provide all the necessary information
	let newUser = new User({
		firstName : reqBody.firstName,
		lastName : reqBody.lastName,
		mobileNo : reqBody.mobileNo,
		email : reqBody.email,
		isAdmin : reqBody.isAdmin,
		password : bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((user, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}

// GET SPECIFIC USER DETAILS
module.exports.retrieveUser = (data) => {

	return User.findById(data.userId).then(result => {
		result.password = "";
		return result;
	});
};

// RETRIEVE ALL ACTIVE PRODUCTS 
module.exports.retrieveUsers = () => {
	return User.find().then(result => {
		return result;
	})
};


module.exports.getProfile = (data) => {

	return User.findById(data.userId).then(result => {

		// Changes the value of the user's password to an empty string when returned to the frontend
		// Not doing so will expose the user's password which will also not be needed in other parts of our application
		// Unlike in the "register" method, we do not need to call the mongoose "save" method on the model because we will not be changing the password of the user in the database but only the information that we will be sending back to the frontend application
		
		result.password = "";

		// Returns the user information with the password as an empty string
		return result;
	});
};


// LOGIN REQUEST & GENERATE TOKEN
module.exports.loginUser = (reqBody) => {

	return User.findOne({email: reqBody.email}).then(result => {
		if(result == null){
			return false;
		}
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			
			if(isPasswordCorrect){
				return { access: auth.createAccessToken(result) }

			}	
			else{
				return false;
			}
		}
	})
}

// ADMIN REGISTRATION (AN ADMIN ONLY FEATURE)
module.exports.setAdmin = async (userData, reqParams) => {
	if(userData.isAdmin){
		return User.findByIdAndUpdate({_id: reqParams.userId}, {isAdmin: true}).then(result =>{
			return result.save().then((updatedResult, error) =>{
				if(error){
					return false;
				} else{
					return true;
				}
			})
		})
	} else{
		return false;
	}
}

// ORDER A PRODUCT
// Async await will be used in ordering a product because we will need to update two separate documents when buying a product:
module.exports.buyProduct = async (userData, reqBody) => {
	if(userData.isAdmin == false){
	// Add the order ID in the orders array of the user 
		let isUserUpdated = await User.findById(data.userId).then(user => {
			// Adds the orderId in the user's orders array
			user.Orders.push({orderId : data.orderId});

			// Saves the updated user information in the database
			return user.save().then((user, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

		// Add the order ID in the thoseWhoOrdered array of the product
		let isProductUpdated = await Product.findById(data.productId).then(product => {
			// Adds the orderId in the product's thoseWhoOrdered array
			product.thoseWhoOrdered.push({orderId : data.orderId});

			// Saves the updated product information in the database
			return product.save().then((product, error) => {
				if(error){
					return false;
				}
				else{
					return true;
				}
			})
		})

		// Condition that will check if the user and product documents have been updated
		if(isUserUpdated && isProductUpdated){
			return true;
		}
		else{
			return false;
		}
	}else{
		return false;
	}
};


