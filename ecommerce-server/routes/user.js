// USER ROUTES
const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const orderController = require("../controllers/order");
const auth = require("../auth");


// Route for checking if the user's email already exists in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});



// ROUTE FOR REGISTER USER
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});

// Route for retrieving user details
// auth.verify method acts as a middleware to ensure that the user is logged in before they can access specific app features
router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));
});

// ROUTE FOR GET SPECIFIC USER DETAILS
router.get("/details", (req, res) => {
	userController.retrieveUser({userId : req.body.userId}).then(resultFromController => res.send(resultFromController));
});

// ROUTE FOR RETRIEVE ALL USERS
router.get("/", (req, res) => {
	userController.retrieveUsers(req.body).then(resultFromController => res.send(resultFromController));
});



// ROUTE FOR LOGIN REQUEST & GENERATE TOKEN
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// ROUTE FOR ADMIN REGISTRATION (AN ADMIN ONLY FEATURE)
router.put("/:userId/setAsAdmin", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	userController.setAdmin(userData, req.params).then(resultFromController => res.send(resultFromController));
	console.log(req.params);
	console.log(userData);
});


// ROUTE TO ORDER A PRODUCT (A NON-ADMIN ONLY FEATURE)
router.post("/buy", auth.verify, (req, res) => {
	let data = {
		userId : req.body.userId,
		productId : req.body.productId
	}
	const userData = auth.decode(req.headers.authorization);
	userController.buyProduct(userData, data).then(resultFromController => res.send(resultFromController));
});







// Allows us to export the "router" object that will be accessed in "index.js"\
module.exports = router;