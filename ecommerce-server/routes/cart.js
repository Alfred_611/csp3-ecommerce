const express = require ("express");
const router = express.Router();
// const userController = require("../controllers/user");
const cartController = require("../controllers/cart");
const auth = require("../auth");

router.post("/myCart", async (req, res) => {
	cartController.getUserCart(req.body).then(resultFromController => res.send(resultFromController));
})
module.exports = router;