// ORDER MODEL
const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	
	productName : {
		type: String
	},

	productId : {
		type: String
	},


	userId : {
		type: String
	},


	totalAmount : {
		type : Number
	},


	purchasedOn : {
		type: Date,
		default: new Date()
	}
});

module.exports = mongoose.model("Order", orderSchema);