const mongoose = require("mongoose");

// Create user with the following properties
const cartSchema = new mongoose.Schema({
	userId :{
		type: String,
	},

	products: [{

		productId : {
			type: String,
			required:[true, "User id is required"]
		},
		productName : {
			type: String,
			required:[true, "User id is required"]
		},
		price : {
			type: String,
			required:[true, "User id is required"]
		},
		subTotal : {
			type: Number,
			required: [true, "User id is required"]
		}
	}],
	total: {
		type: Number
	}
});

module.exports = mongoose.model("Cart", cartSchema);