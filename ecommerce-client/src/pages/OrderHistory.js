import { Table, Container, Row, } from 'react-bootstrap';
import { Fragment, useEffect, useState, useContext } from 'react';
import OrderTable from '../components/OrderTable';
import UserContext from '../UserContext';


export default function OrderHistory(){
	const {user, setUser} = useContext(UserContext);
	const [orders, setOrders] = useState ([]);


	useEffect(() => {
		fetch('http://localhost:4000/users/MyOrders', {
			method: 'GET',
			headers: {
				'Content-Type': 'application/json'
			}


		})
		.then(res => res.json())
		.then(data => {
			console.log(data);


if(typeof data.access !== "undefined"){
		 		// The token will be used to retrieve user information across the whole frontend application and storing it in the localStorage to allow ease of access to the user's information
		 		localStorage.setItem('token', data.access);

		 		retrieveUserDetails(data.access);
			}



const retrieveUserDetails = (token) => {
		fetch('http://localhost:4000/users/details', {
			headers: {
				Authorization: `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}
		
// Sets the "products" state to map the data retrieved from the fetch request in several "ProductCard Components"
			setOrders(data.map(order => {
					return (
						<OrderTable key={order.id} orderProp={order} />
					)
				})
			);		
		})
	}, [])
		

return (
			
		<Container>
		<Fragment> 
		<h1 className="mt-3 mb-4">My Orders</h1>
		</Fragment>
		<Row>

		<Fragment> 
			{orders}
		</Fragment>
		</Row>
		</Container>
	)
}




