import { Fragment } from 'react';
import Banner from '../components/Banner';

import { Container } from 'react-bootstrap';

export default function Error () {

	const data = {
		title: "404 - Page not found",
		content: "The page you're looking for cannot be found",
		destination: "/",
		label: "Go back home"
	}

	return(
		<Fragment>
			<Container className="home">
			<Banner data={data}/>
			</Container>
		</Fragment>

		)

}