import { Table, Button, InputGroup } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
export default function Cart(){
		
	return (
		<div>
		<h2 className="mt-4">My Cart</h2>
		<Table striped bordered hover className="mt-2">
		  <thead>
		    <tr>
		      <th><div className="d-flex justify-content-center">Name</div></th>
		      <th><div className="d-flex justify-content-center">Price</div></th>
		      <th><div className="d-flex justify-content-center">Quantity</div></th>
		      <th><div className="d-flex justify-content-center">Subtotal</div></th>
		      <th></th>
		    </tr>
		  </thead>
		  <tbody>
		    <tr>
		      <td><div className="d-flex justify-content-center">placer</div></td>
		      <td><div className="d-flex justify-content-center">placer</div></td>
		      <td><div className="d-flex justify-content-center">dito yung +- sa quantity</div></td>
		      <td><div className="d-flex justify-content-center">placer</div></td>
		      <td><div className="d-flex justify-content-center"><Button variant="warning" type="submit">Remove</Button></div></td>
		    </tr>
		    <tr>
		      <td><div className="d-flex justify-content-center">placer</div></td>
		      <td><div className="d-flex justify-content-center">placer</div></td>
		      <td><div className="d-flex justify-content-center">dito yung +- sa quantity</div></td>
		      <td><div className="d-flex justify-content-center">placer</div></td>
		      <td><div className="d-flex justify-content-center"><Button variant="warning" type="submit">Remove</Button></div></td>
		    </tr>
		    <tr>
		      <td colSpan="3"><div className="d-flex justify-content-center"><Button variant="warning" type="submit">Checkout</Button></div></td>
		      <td colSpan="2">Total: </td>
		    </tr>
		  </tbody>
		</Table>
		</div>

	)
}