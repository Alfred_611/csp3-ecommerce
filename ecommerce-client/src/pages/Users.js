import { Fragment, useEffect, useState } from 'react';
import UserCard from '../components/UserCard';
import { Container, Row, Button, Col, Table } from 'react-bootstrap';


export default function Users() {
	const [users, setUsers] = useState ([]);


	useEffect(() => {
		fetch('http://localhost:4000/users')
		.then(res => res.json())
		.then(data => {
			console.log(data);

		
// Sets the "products" state to map the data retrieved from the fetch request in several "ProductCard Components"
			setUsers(data.map(user => {
					return (
						<UserCard key={user.id} userProp={user} />
					)
				})
			);		
		})
	}, [])


return (

		<Container>
		<Fragment> 
		<h1 className="mt-3 mb-4">Users</h1>
		</Fragment>
		<Row>

		<Fragment> 
			{users}
		</Fragment>
		</Row>
		</Container>
	)
}