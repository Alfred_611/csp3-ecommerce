import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login(props){
    // Allows us to consume the User context object and it's properties to use for user validation 
    const {user, setUser} = useContext(UserContext);
	// State hooks to store the values of the input fields
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);

	console.log(email);
	console.log(password);

	// Function to simulate user registration
	function authenticate(e){
		e.preventDefault();
		// Fetch request to process the backend API
		// Syntax: fetch('url', {options})
		// .then(res => res.json())
		// .then(data => {})
		fetch('http://localhost:4000/users/login', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
		// 	// If no user information is found, the "access" property will not be available and will return undefined
		if(typeof data.access !== "undefined"){
		 		// The token will be used to retrieve user information across the whole frontend application and storing it in the localStorage to allow ease of access to the user's information
		 		localStorage.setItem('token', data.access);

		 		retrieveUserDetails(data.access);

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "You may now view our products!"
				})
			}
			else{
				Swal.fire({
					title: "Authentication failed",
					icon: "error",
					text: "Check your login details and try again."
				})
			}
		})

		// Set the email of the authenticated user in the local storage
		// Syntax: localStorage.setItem('propertyName', value);
		/*localStorage.setItem('email', email);*/

		// Set the global user state to have properties obtained from our local storage
		/*setUser({
			email: localStorage.getItem('email')
		});*/

		setEmail('');
		setPassword('');

			//alert('You are now logged in.')
	}

	const retrieveUserDetails = (token) => {
		fetch('http://localhost:4000/users/details', {
			headers: {
				Authorization: `Bearer ${ token }`
			}
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			setUser({
				id: data._id,
				isAdmin: data.isAdmin
			})
		})
	}

	useEffect(() => {
		// Validate to enable submit button when all fields are populated and both passwords match 
		if((email !== '' && password !==''))
		{
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [email, password])
	
		return(
			(user.id !== null) ?
			(user.isAdmin === true) ?

				<Redirect to="/admin" />
				:
				<Redirect to="/products" />
			:
			<Form onSubmit={(e) => authenticate(e)}>
			{/*Bind the input states via 2-way binding*/}
			<h2 className="mt-4 mb-1">Login</h2>
			  <Form.Group controlId="userEmail">
			    <Form.Label className="mt-4 mb-1">Email address</Form.Label>
			    <Row>
				<Col xs={4}>
			    <Form.Control size="sm" 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value = {email}
			    	onChange = { e => setEmail(e.target.value)}
			    	required />
			    </Col>
			    </Row>
			  </Form.Group>

			  <Form.Group controlId="password">
			    <Form.Label className="mt-4 mb-1">Password</Form.Label>
			    <Row>
				<Col xs={3}>
			    <Form.Control size="sm"
			     	type="password" 
			     	placeholder="Password" 
			     	value={password}
			     	onChange={e => setPassword(e.target.value)}
			     	required />
			    </Col>
			    </Row>
			  </Form.Group>
				{/*Conditionally render the submit button based on isActive state*/}
				{ isActive ?
					<Button className="mt-4 mb-1" variant="outline-success" type="submit" id="submitBtn">
			 		   Login
			 		</Button>
			 		:
			 		<Button className="mt-4 mb-1" variant="outline-success" type="submit" id="submitBtn" disabled>
			  		   Login
			  		</Button>
				 }  
			</Form>
		)	
}