import { useState, useEffect, useContext } from 'react';
import { Form, Button, Row, Col } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { useHistory } from 'react-router-dom';

export default function Register(){
	// State hooks to store the values of the input fields
	const [firstName, setFirstName] = useState('');
	const [lastName, setLastName] = useState('');
	const [email, setEmail] = useState('');
	const [mobile, setMobile] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	// State to determine whether the submit button is enabled or not
	const [isActive, setIsActive] = useState(false);
	const {user, setUser} = useContext(UserContext);
	let history = useHistory();

	console.log(email);
	console.log(password1);
	console.log(password2);

	// Function to simulate user registration
	function registerUser(e){
  e.preventDefault();
  fetch('http://localhost:4000/users/checkEmail',{
    method: 'POST',
    headers:{
        'Content-Type':'application/json'
    },
    body: JSON.stringify({
        email:email
    })
  })
  .then(res => res.json())
  .then(data => {

    if(data === false){
      fetch('http://localhost:4000/users/register',{
        method: 'POST',
        headers:{
            'Content-Type':'application/json'
    },
		body: JSON.stringify({
            firstName:firstName,
            lastName:lastName,
            email:email,
            password:password1,
            mobileNo:mobile
        })
      })
      .then(res => res.json())
      .then(data => {
        console.log(`${data}dataDATA`);
        Swal.fire({
            title:"Registration Successful",
            icon:"success",
            text:"Please login to access the site."
        });
        history.push('/login')
      })
}
    else{
      Swal.fire({
          title:"Duplicate email found",
          icon:"error",
          text:"Please provide a different email."
      })
    }
  })
	setEmail('');
  setPassword2('');
  setPassword1('');
  setFirstName('');
  setLastName('');
  setMobile('');

}

	useEffect(() => {
		// Validate to enable submit button when all fields are populated and both passwords match 
		if((firstName !== '' && lastName !== '' && email !== '' && mobile !== '' && password1 !=='' && password2 !=='') && (password1 === password2))
		{
			setIsActive(true);
		}
		else{
			setIsActive(false);
		}
	}, [firstName, lastName, email, mobile, password1, password2])

	return (	
		(user.id !== null) ?
		 	<Redirect to="/login" />
		 	:
		<Form onSubmit={(e) => registerUser(e)}>

			{/*Bind the input states via 2-way binding*/}

				<h2 className="mt-4 mb-1">Register</h2>
				<Form.Group controlId="userFirstName">
			    <Form.Label className="mt-4 mb-1">First Name</Form.Label>
			    <Row>
				<Col xs={4}>
			    <Form.Control size="sm"
			     	type="firstname" 
			     	placeholder="Enter first name" 
			     	value={firstName}
			     	onChange={e => setFirstName(e.target.value)}
			     	required />
			    </Col>
			    </Row>
			  	</Form.Group>



			  	<Form.Group controlId="userLastName">
			    <Form.Label className="mt-4 mb-1">Last Name</Form.Label>
			    <Row>
				<Col xs={4}>
			    <Form.Control size="sm"
			     	type="lastname" 
			     	placeholder="Enter last name" 
			     	value={lastName}
			     	onChange={e => setLastName(e.target.value)}
			     	required />
			    </Col>
			    </Row>
			  	</Form.Group>



			  	<Form.Group controlId="userEmail">
			    <Form.Label className="mt-4 mb-1">Email address</Form.Label>
			    <Row>
				<Col xs={4}>
			    <Form.Control size="sm" 
			    	type="email" 
			    	placeholder="Enter email" 
			    	value = {email}
			    	onChange = { e => setEmail(e.target.value)}
			    	required />
			    </Col>
			    </Row>
			  	</Form.Group>



			  	<Form.Group controlId="userMobileNumber">
			    <Form.Label className="mt-4 mb-1">Mobile Number</Form.Label>
			    <Row>
				<Col xs={3}>
			    <Form.Control size="sm"
			     	type="mobilenumber" 
			     	placeholder="Enter mobile number" 
			     	value={mobile}
			     	onChange={e => setMobile(e.target.value)}
			     	required
			     	minlength ="11" />
			     </Col>
			    </Row>
			  	</Form.Group>



			  	<Form.Group controlId="password1">
			    <Form.Label className="mt-4 mb-1">Password</Form.Label>
			    <Row>
				<Col xs={3}>
			    <Form.Control size="sm"
			     	type="password" 
			     	placeholder="Password" 
			     	value={password1}
			     	onChange={e => setPassword1(e.target.value)}
			     	required />
			    </Col>
			    </Row>
			  	</Form.Group>



			  	<Form.Group controlId="password2">
			  	<Form.Label className="mt-4 mb-1">Verify Password</Form.Label>
			  	<Row>
				<Col xs={3}>
			    <Form.Control size="sm" 
			    	type="password" 
			    	placeholder="Verify Password" 
			    	value={password2}
			     	onChange={e => setPassword2(e.target.value)}
			    	required />
			    </Col>
			    </Row>
			  	</Form.Group>



				{/*Conditionally render the submit button based on isActive state*/}
				{ isActive ?
					<Button className="mt-4 mb-1" variant="outline-success" type="submit" id="submitBtn">
			 		   Register
			 		</Button>
			 		:
			 		<Button className="mt-4 mb-1" variant="outline-success" type="submit" id="submitBtn" disabled>
			  		   Register
			  		</Button>
				 }
				  
		</Form>
	)
}