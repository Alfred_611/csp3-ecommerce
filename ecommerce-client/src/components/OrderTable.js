import PropTypes from 'prop-types';
import { Table } from 'react-bootstrap';
import { useState, useEffect, Fragment } from 'react';
import React from 'react';
import Swal from 'sweetalert2';


export default function OrderTable({orderProp}) {
	const {_id,purchasedOn, productName, totalAmount} = orderProp;
	function addToCart(e){
		
		console.log(orderProp);
	}
		return (
			<div>
					<h2 className="mt-4">My Order History</h2>
					<Table striped bordered hover className="mt-2">
					  <thead>
					    <tr>
					      <th><div className="d-flex justify-content-center">Order ID</div></th>
					      <th><div className="d-flex justify-content-center">Purchase Date</div></th>
					      <th><div className="d-flex justify-content-center">Item/s</div></th>
					      <th><div className="d-flex justify-content-center">Total</div></th>
					    </tr>
					  </thead>
					  <tbody>
					    <tr>
					      <td>{_id}</td>
					      <td>{purchasedOn}</td>
					      <td>{productName}</td>
					      <td>{totalAmount}</td>
					    </tr>
					  </tbody>
					</Table>
			</div>
			)
};