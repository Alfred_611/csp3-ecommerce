import { Fragment, useContext } from 'react';
// Import necessary components from react-bootstrap
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink } from 'react-router-dom';
import UserContext from '../UserContext';

// AppNavbar component 
export default function AppNavbar(){
	// State to store the user information stored in the login page
	// const [user, setUser] = useState(localStorage.getItem("email"));
	// console.log(user);

	const {user} = useContext(UserContext);

	return(

		(user.isAdmin === true) ?
			<Navbar bg="dark" variant="dark">
		    	<Navbar.Brand as={NavLink} to="/" exact>CJ Music Shop</Navbar.Brand>
		     		<Navbar.Toggle aria-controls="basic-navbar-nav" />
			 			<Navbar.Collapse id="basic-navbar-nav">
			    				<Nav className="ml-auto">

			    					<Nav.Link as={NavLink} to="/admin"        exact>Dashboard</Nav.Link>
			    					<Nav.Link as={NavLink} to="/logout"        exact>Logout</Nav.Link>

			    				</Nav>
			    	</Navbar.Collapse>
			</Navbar>:		    				


			<Navbar bg="dark" variant="dark">

		    	<Navbar.Brand as={NavLink} to="/" exact>CJ Music Shop</Navbar.Brand>
		     		<Navbar.Toggle aria-controls="basic-navbar-nav" />
			 			<Navbar.Collapse id="basic-navbar-nav">
			    				<Nav className="ml-auto">
			      					

			      					{(user.id !== null) ?
			      					  <Fragment>
			      					  <Nav.Link as={NavLink} to="/cart"          exact>My Cart</Nav.Link>
			      					  <Nav.Link as={NavLink} to="/order-history" exact>My Order History</Nav.Link>
			      					  <Nav.Link as={NavLink} to="/logout"        exact>Logout</Nav.Link>
			      					  </Fragment>
			      					  :
			      					  <Fragment>
			      					  	<Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
			      						<Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
			      					  </Fragment>
			      					}
			      					
			    				</Nav>
			  			</Navbar.Collapse>
		  	</Navbar>
	);
}
